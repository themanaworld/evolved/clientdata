# Contributing

All contributions must adhere to the project license (GPLv3 and/or CC BY SA 4.0).

By submiting any patch, in any means, or content for the game you agree with the
contribution terms.

More information at https://forums.themanaworld.org/viewtopic.php?f=1&t=1177
